# TODO
# DASHBOARD
# VOUCH
# NOTIFICATION
# LOGIN
# CREATEUSER
# ADMIN DASHBOARD


from flask import Flask, render_template, request, make_response, redirect, url_for
import base64, json, functools, psycopg2, bcrypt
from psycopg2.pool import SimpleConnectionPool
from contextlib import contextmanager

app = Flask(__name__)

dbConnection = "dbname='tillit' user='postgres' host='localhost' password='postgres'"
connectionpool = SimpleConnectionPool(1,10,dsn=dbConnection)

@contextmanager
def get_db_connection():
    con = connectionpool.getconn()
    try:
        yield con
    finally:
        connectionpool.putconn(con)

def login_required(func):
    @functools.wraps(func)
    def wrapper_login_required(*args, **kwargs):
        if get_cookie_val(request.cookies.get('tillit_cookie')) is None:
            return redirect(url_for("login", next=request.url))
        return func(*args, **kwargs)
    return wrapper_login_required

def get_cookie_val(cookie_string):
    if cookie_string is not None and cookie_string != "":
        return json.loads(base64.b64decode(cookie_string))
    return None

@app.route('/')
def hello_world():
    cookie_val = get_cookie_val(request.cookies.get('tillit_cookie'))
    if cookie_val is None:
        return render_template('index.html', is_logged_in=False)
    else:
        return render_template('index.html', is_logged_in=True,username = cookie_val["username"])

def authenticate(user, password):
    with get_db_connection() as conn:
        sqlstr = "SELECT password, user_id from users where username='" + user +"'"
        cur = conn.cursor()
        cur.execute(sqlstr)
        row = cur.fetchone()
        return row[1], bcrypt.hashpw(str(password), row[0]) == row[0]

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        user_id, auth_response = authenticate(request.form['username'], request.form['password'])
        if auth_response:    
            resp = make_response(redirect(url_for('hello_world')))
            resp.set_cookie('tillit_cookie', base64.b64encode(json.dumps({"username": request.form['username'], "user_id": user_id })))
            return resp
        return redirect(url_for('login', is_logged_in=False))
    return render_template('login.html', is_logged_in=False)

@app.route('/temp')
def temp():
    return render_template('base.html')

@app.route('/listideas')
def listideas():
    cookie_val = get_cookie_val(request.cookies.get('tillit_cookie'))
    if cookie_val is None:
        is_logged_in=False
        username = ""
    else:
        is_logged_in = True
        username = cookie_val["username"]
    with get_db_connection() as conn:
        sqlstr = "SELECT s.uname, s.title, s.desc, count(v.user_id), s.idea_id from vouch v RIGHT join (SELECT  a.idea_id as idea_id, u.username as uname, a.title as title, a.description as desc from application a inner join users u on a.user_id = u.user_id ORDER by updated desc) as s on v.application_id = s.idea_id group by s.idea_id, s.uname, s.title, s.desc"
        cursor = conn.cursor()
        cursor.itersize = 200
        cursor.execute(sqlstr)
        ideas = []
        for row in cursor:
            ideas.append({'username': row[0], 'title': row[1], 'description': row[2], 'vouches': row[3], 'idea_id': row[4]})
    return render_template('list.html', ideas=ideas, title="All Applications", is_logged_in=is_logged_in, username=username)

@app.route('/dashboard')
@login_required
def dashboard():
    cookie_val = get_cookie_val(request.cookies.get('tillit_cookie'))
    if cookie_val is None:
        is_logged_in=False
        username = ""
    else:
        is_logged_in = True
        username = cookie_val["username"]
    with get_db_connection() as conn:
        sqlstr = "SELECT s.uname, s.title, s.desc, count(v.user_id) from vouch v RIGHT join (SELECT  a.idea_id as idea_id, u.username as uname, a.title as title, a.description as desc from application a inner join users u on a.user_id = u.user_id and u.username ='" +str(cookie_val["username"]) + "' ORDER by updated desc) as s on v.application_id = s.idea_id group by s.idea_id, s.uname, s.title, s.desc"
        cursor = conn.cursor()
        cursor.itersize = 200
        cursor.execute(sqlstr)
        ideas = []
        for row in cursor:
            ideas.append({'username': row[0], 'title': row[1], 'description': row[2], 'vouches': row[3], 'idea_id': row[4]})
    return render_template('list.html', ideas=ideas, title="Your Applications", is_logged_in=is_logged_in, username=username)

@app.route('/idea-<idea_id>')
def view(idea_id):
    cookie_val = get_cookie_val(request.cookies.get('tillit_cookie'))
    if cookie_val is None:
        is_logged_in=False
        username = ""
    else:
        is_logged_in = True
        username = cookie_val["username"]
    with get_db_connection() as conn:
        sqlstr = "SELECT s.uname, s.title, s.desc, count(v.user_id), s.idea_id, s.user_id from vouch v RIGHT join (SELECT  a.idea_id as idea_id, u.username as uname, a.title as title, a.description as desc, u.user_id as user_id from application a inner join users u on a.user_id = u.user_id and a.idea_id ='" +str(idea_id) + "' ORDER by updated desc) as s on v.application_id = s.idea_id group by s.idea_id, s.uname, s.title, s.desc, s.user_id"
        cursor = conn.cursor()
        cursor.execute(sqlstr)
        row = cursor.fetchone()
        hashstring= base64.b64encode(str(idea_id)+"-"+str(row[5]))
        is_mine= row[0] == cookie_val["username"]
        idea = {'username': row[0], 'title': row[1], 'description': row[2], 'vouches': row[3], 'idea_id': row[4], 'hash': hashstring, 'user_id': row[5]}
    with get_db_connection() as conn2:
        sqls = "SELECT u.vouch_score, u.user_id from vouch v INNER JOIN users u on u.user_id = v.user_id  where v.application_id =" + idea_id
        cursor2 = conn2.cursor()
        cursor2.execute(sqls)
        is_vouched = False
        sum_vouch = 0
        count = 0
        for row in cursor2:
            sum_vouch = sum_vouch + row[0]
            count = count + 1
            if row[1] == cookie_val["user_id"]:
                is_vouched =True
                break
        if count != 0:
            loan_amount_1 = 10000. * sum_vouch/count*23
            interest_amount_1 = 40.*count*20/sum_vouch
        
            loan_amount_2 = 100000. * sum_vouch+10/count+10
            interest_amount_2 = 40.* (count+8)/(sum_vouch+8)
        else:
            loan_amount_1=interest_amount_1=loan_amount_2=interest_amount_2 =0
    return render_template('view.html', idea=idea, is_mine=is_mine, is_vouched=is_vouched, is_logged_in=is_logged_in, username=username, loan_amount_1=loan_amount_1, interest_amount_1=interest_amount_1, loan_amount_2=loan_amount_2, interest_amount_2=interest_amount_2)

@app.route('/vouch/<hashid>')
def vouch(hashid):
    decoded = base64.b64decode(hashid)
    idea_id, user_id = decoded.split('-')
    with get_db_connection() as conn:
        sqlstr= "INSERT INTO vouch values("+ idea_id +","+ user_id +", now())"
        cursor = conn.cursor()
        cursor.execute(sqlstr)
        conn.commit()
    return redirect(url_for('view', idea_id=idea_id))

